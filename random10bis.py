import random
import time
import requests
import json
import browser_cookie3 as browsercookie
from bs4 import BeautifulSoup
import webbrowser


DOMAIN = 'www.10bis.co.il'
SITE = "https://{}".format(DOMAIN)
SEARCH_RESTAURANTS_URL = '{}/Restaurants/SearchRestaurants'.format(SITE)
GET_MENU_URL = '{}/Restaurants/Menu/Delivery'.format(SITE)
GET_DISH_URL = '{}/Menu/Dish'.format(SITE)
ADD_DISH_URL = '{}/ShoppingCart/AddDishAjax'.format(SITE)
GET_DISHES_URL = '{}/ShoppingCart/DishesIndex'.format(SITE)
GET_TOTAL_BILL = '{}/Checkout/GetShoppingCartTotalToCharge'.format(SITE)


def setup_session():
    session = requests.session()
    # For debug purposes
    session.verify = False
    # Set chrome cookies in order to avoid implementing login
    cj = browsercookie.chrome()
    try:
        cookies = cj._cookies[DOMAIN]['/']
    except KeyError:
        print("YOU ARE NOT LOGGED IN TO 10BIS")
        raise
    for cookie in cookies.values():
        session.cookies.set(cookie.name, cookie.value)
    return session


def get_user_id(session):
    response = session.get(SITE)
    user_id = response.url.split('/')[-1]
    return user_id


def get_random_restaurant(session):
    date_format = '%d/%m/%Y %H:%M:%S'
    time_str = time.strftime(date_format)
    restaurants = session.get(SEARCH_RESTAURANTS_URL,
                              params=dict(deliveryMethod='Delivery', ShowOnlyOpenForDelivery=True, pageSize=9999,
                                          id=get_user_id(session), desiredDateAndTime=time_str))
    restaurants_json = json.loads(restaurants.content)

    random_restaurant = random.choice(restaurants_json)
    return random_restaurant


def get_random_dish(session, res):
    res_id = res['RestaurantId']
    restaurant_menu = session.get(GET_MENU_URL, params=dict(ResId=res_id))
    soup = BeautifulSoup(restaurant_menu.content, 'html.parser')
    dishes = soup.findAll("div", {"class": "dishesBox"})
    random_dish = random.choice(dishes)
    random_dish = session.get(GET_DISH_URL, params=dict(categoryId=random_dish.get("data-categoryid"),
                                                        dishId=random_dish.get("data-dishid")))
    random_dish = json.loads(random_dish.content)
    return random_dish


def add_dish(session, dish):
    dish_json = dict()
    if len(dish['Choices']) == 0:
        choices = []
    else:
        choices = []
        for choice in dish['Choices']:
            choices.append(dict(ID=str(choice['ID']), SubsChosen=[]))

    dish_json['dishToSubmit'] = dict(ID=str(dish['DishId']), DishOwnerId=str(dish['CategoryID']), Quantity='1',
                                     DishNotes='',
                                     AssignedUserId=str(dish['UserDishBookmark']['userId']),
                                     Choices=choices)
    headers = {'Content-Type': 'application/json, charset=UTF-8'}

    status = session.post(ADD_DISH_URL, json=dish_json, headers=headers)
    if status.content != b'true':
        print("FAILED")


def open_browser(res):
    res_id = res['RestaurantId']
    webbrowser.open(GET_MENU_URL + '?ResId={}'.format(res_id))


if __name__ == '__main__':
    sess = setup_session()
    rand_res = get_random_restaurant(sess)
    rand_dish = get_random_dish(sess, rand_res)
    add_dish(sess, rand_dish)
    open_browser(rand_res)
